package model.vo;

public class BusUpdateVO {

	int VehicleNo;
	int TripId;
	String RouteNo;
	String Direction;
	String Destination;
	String Pattern;
	double Latitude;
	double Longitude;
	String RecorderedTime;
	Object RouteMap;


	public BusUpdateVO(int pVehiNo, int pTripId, String pRouteNo, String pDirection, String pDestination, 
			String pPattern, double pLatitude, double pLongitud, String pRecorderedTime, Object pRouteMap){

		System.out.println();
		VehicleNo=pVehiNo;
		TripId=pTripId;
		RouteNo=pRouteNo;
		Direction=pDirection;
		Destination=pDestination;
		Pattern=pPattern;
		Latitude=pLatitude;
		Longitude=pLongitud;
		RecorderedTime=pRecorderedTime;
		RouteMap=pRouteMap;

	}
	
	public int darVehiNo(){
		return VehicleNo;
	}
	
	public int darTripId(){
		return TripId;
	}
	
	public String darRouteNo(){
		return RouteNo;
	}
	
	public String darDirection(){
		return Direction;
	}
	
	public String darDestination(){
		return Destination;
	}
	
	public double darLatitude(){
		return Latitude;
	}
	
	public double darLongitude(){
		return Longitude;
	}
	
	public String darRecorderedTime(){
		return RecorderedTime;
	}
	
	public Object darRouteMap(){
		return RouteMap;
	}
	
	public String darMapa2(){
		return RouteMap.toString();
	}
	

}
