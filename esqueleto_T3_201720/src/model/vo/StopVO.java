package model.vo;

public class StopVO {
	
	int stopId;
	int stopCode;
	String stopName;
	String stopDesc;
	double stopLat;
	double stopLon;
	String zoneId;
	String url;
	String locationType;
	String parent;
	
	public StopVO(int pId, int pCode, String pName, String pDesc, double pLat, double pLon, String pZone, String pUrl, String pLocation, String pParent){
		
		stopId=pId;
		stopCode=pCode;
		stopName=pName;
		stopDesc=pDesc;
		stopLat=pLat;
		stopLon=pLon;
		zoneId=pZone;
		url=pUrl;
		locationType=pLocation;
		parent=pParent;
	}
	
	public int darId(){
		return stopId;
	}
	
	public int darCode(){
		return stopCode;
	}
	
	public String darName(){
		return stopName;
	}
	
	public String darDesc(){
		return stopDesc;
	}
	
	public double darLat(){
		return stopLat;
	}
	
	public double darLon(){
		return stopLon;
	}
	
	public String darZone(){
		return zoneId;
	}
	
	public String darUrl(){
		return url;
	}
	
	public String darLocation(){
		return locationType;
	}
	
	public String darParent(){
		return parent;
	}

}
