package model.logic;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.Iterator;
import java.util.Stack;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.stream.JsonReader;

import model.data_structures.IList;
import model.data_structures.IQueue;
import model.data_structures.IStacks;
import model.data_structures.LinkedList;
import model.data_structures.Queue;
import model.data_structures.Stacks;
import model.vo.BusUpdateVO;
import model.vo.StopVO;
import api.ISTSManager;

public class STSManager implements ISTSManager{



	IQueue<BusUpdateVO> buses;
	IList<StopVO> stops;

	public void readBusUpdate(File rtFile) {
		// TODO Auto-generated method stub

		BufferedReader  reader=null;

		try {

			reader= new BufferedReader(new FileReader(rtFile));
			
			JsonReader reader2= new JsonReader(reader);
			Gson gson= new GsonBuilder().create();			
			reader2.beginArray();
			
			buses= new Queue<BusUpdateVO>();
			
			while(reader2.hasNext()){
				BusUpdateVO bus= gson.fromJson(reader2, BusUpdateVO.class);
				buses.enqueue(bus);
			}
	


		} catch (Exception e) {
			// TODO: handle exception
			System.out.println(e.getMessage());
		}

	}

	public IStacks<StopVO> listStops(Integer tripID) {
		// TODO Auto-generated method stub
		IStacks<StopVO> res= new Stacks<StopVO>(); 
		Iterator<BusUpdateVO> iter= buses.iterator();
		BusUpdateVO aBuscar=null;

		while(iter.hasNext()&& aBuscar==null){	

			BusUpdateVO actual=iter.next();
			if(actual.darTripId()==tripID){
				aBuscar=actual;
			}
			buses.dequeue();
		}

		if(aBuscar!=null){
			loadStops();
			Iterator<StopVO> iter2=stops.iterator();
			while(iter2.hasNext()){

				StopVO actual=iter2.next();
				double distance=getDistance(aBuscar.darLatitude(), aBuscar.darLongitude(), actual.darLat(), actual.darLon());
				if(distance<=70){
					res.push(actual);
				}
			}
		}

		return res;
	}

	public double getDistance(double lat1, double lon1, double lat2, double lon2) {
		// TODO Auto-generated method stub
		final int R = 6371*1000; // Radious of the earth

		Double latDistance = toRad(lat2-lat1);
		Double lonDistance = toRad(lon2-lon1);
		Double a = Math.sin(latDistance / 2) * Math.sin(latDistance / 2) + 
				Math.cos(toRad(lat1)) * Math.cos(toRad(lat2)) * 
				Math.sin(lonDistance / 2) * Math.sin(lonDistance / 2);
		Double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
		Double distance = R * c;

		return distance;

	}

	private Double toRad(Double value) {
		return value * Math.PI / 180;
	}



	public void loadStops() {
		// TODO Auto-generated method stub
		stops =  new LinkedList<StopVO>();
		
			try
			{
				BufferedReader bf = new BufferedReader( new FileReader( "data/stops.txt" ) );
	            String line = bf.readLine( );
	            bf.close();

	            while(line!=null)
	            {
	            	String[] parts= line.split(",");
	            	int stop_id=Integer.parseInt(parts[0].trim());
	            	int stopCode=Integer.parseInt(parts[1].trim());
	            	String stopName=parts[2].trim();
	            	String stopDesc=parts[3].trim();
	            	double stopLat=Double.parseDouble(parts[4].trim());
	            	double stopLon=Double.parseDouble(parts[4].trim());
	            	String stopZone=parts[6].trim();
	            	String url=parts[7].trim();
	            	String location=parts[8].trim();
	            	String parent=parts[9].trim();

	            	StopVO newStop= new StopVO(stop_id, stopCode, stopName, stopDesc, stopLat, stopLon, stopZone, url, location, parent);
	            	stops.add(newStop);	            	
	            }


			} catch (Exception e) {
				// TODO: handle exception
			}


	}

}
