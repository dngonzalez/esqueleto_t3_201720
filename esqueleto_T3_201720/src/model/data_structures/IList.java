package model.data_structures;

import java.util.Iterator;

/**
 * Abstract Data Type for a list of generic objects
 * This ADT should contain the basic operations to manage a list
 * add, addAtEnd, AddAtK, getElement, getCurrentElement, getSize, delete, deleteAtK
 * next, previous
 * @param <T>
 */
public interface IList<T> extends Iterable<T> {
	
	public void add(T object);
	
	public void addAtEnd(T object);
	
	public void addAtK(T object, int K);
	
	public T getElement(int k);
	
	public T getCurrentElement();
	
	public void next();
	
	public void previous();
	
	public long getSize();
	
	public T delete(T object);
	
	public T deleteAtK(int k);

	Iterator<T> iterator();
}
